#include<iostream>

using namespace std;

void boardDisplay(int[6][7]);
void chipInput(int [6][7], int, int);
void turnChange (int &);
void checkChip(int[6][7], int, int, int &, int &, int &, bool &);
void boardCheck_Horizontal(int[6][7], int &, bool &);
void boardCheck_Vertical(int[6][7], int &, bool &);
void boardCheck_DiagonalLEFT(int[6][7], int &, bool &);
void boardCheck_DiagonalRIGHT(int[6][7], int &, bool &);

int main()
{
	const int rowMax = 6, columnMax = 7;
	int board[rowMax][columnMax] = 
	{{0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0},
	{0, 0, 0, 0, 0, 0, 0}};
	bool victory = false;
	int player = 1, round = 1, victor;
	
	while(true)
	{
		
		/* Section: Display & Input
			- If there was a victor, the endgame would be declaed here, followed by end program.*/
		
		boardDisplay(board);
		if(victory)
			{
				if(victor == 3)
				{
					cout << endl << "Draw!";
				}
				else
				{
					cout << endl << "Player " << victor << " wins!";
				}
				return 0;
			}
		chipInput(board, player, columnMax);
		round++;
		
		/* Section: Board checking 
			- This section will only work after round 7 (player 1 has placed 4 chips)
			- If the game reached round 43 and there's no winner, the game would declared draw in Display 7 Input section.*/
		
		if(round >= 7)
		{			
			boardCheck_Horizontal(board, victor, victory);
			boardCheck_Vertical(board, victor, victory);
			boardCheck_DiagonalLEFT(board, victor, victory);
			boardCheck_DiagonalRIGHT(board, victor, victory);
			if(round == 43)
			{
			victory = true;
			victor = 3;	
			}
		}
		
		/* Section: End turn 
			- Change player. */
		turnChange(player);
	}
	return 0;	
}

void boardDisplay(int board[][7])
{
	/* Clear the screen, and then rewrite the whole display. (as adviced by Ronnakit)*/
	
	system("cls");
	cout << "---- Connect 4! ----" << endl << endl;
	for(int i = 1; i <= 7; i++)
	{
		cout << " " << i << " ";
	}
	cout << endl << endl;
	for(int i = 0; i < 6; i++)
		{
			for(int j = 0; j < 7; j++)
			{
				switch(board[i][j])
				{
					case 0: cout << "[ ]"; break;
					case 1: cout << "[1]"; break;
					case 2: cout << "[2]"; break;
				}
			}
			cout << endl;
		}
}

void chipInput(int board[][7], int player, int columnMax)
{
	/* Evaluation steps: 
		1. Validate input
		2. Check column availablity
		3. Actual input */
	
	bool validInput = false, chipDropped = false;
	int input, k;
	
	while(!validInput)
	{
		cout << endl << "Player " << player << ": ";
		cin >> input;
		if(input < 1 || input > columnMax)							//Step 1
		{
			cout << "Invalid Input!" << endl;
			validInput = false;	
		}
		else if(board[0][input-1] == 1 || board[0][input-1] == 2)	//Step 2
		{
			cout << "Column is full!" << endl;
			validInput = false;
		}
		else														//Step 3
		{
			k = 5;
			chipDropped = false;
			
			while(!chipDropped)
			{
				if(board[k][input-1] == 0)
				{
					board[k][input-1] = player;
					chipDropped = true;
				}
				else
				{
					k--;
				}
			}
			validInput = true;
		}
	}
}

void turnChange(int &player)
{
	switch(player)
	{
		case 1: player = 2; break;
		case 2: player = 1; break;
	}
}

void checkChip(int board[][7], int row, int column, int &number, int &stacks, int &victor, bool &victory)
{
	/* How it works:
		- If the current position is empty, stack = 0
		- If the current position is the same as previous, stack++
		- If the current position is different than the previous, recount stack
		- If the stack reaches 4, declare victor */
		
	if(board[row][column] == 0)
	{
		stacks = 0;
	}
	else if(board[row][column] == number)
	{
		stacks++;	
	}
	else
	{
		number = board[row][column];
		stacks = 1;	
	}
	if(stacks == 4)
	{
		victory = true;
		victor = number;	
	}
}

void boardCheck_Horizontal(int board[][7], int &victor, bool &victory)
{
	/* Runs from left to right, top to bottom. */
	
	int row;
	int column;
	int number = 3;
	int stacks = 0;
	for(row = 5; row >= 0; row--)
			{
				for(column = 0; column < 7; column++)
				{
					checkChip(board, row, column, number, stacks, victor, victory);
				}
				stacks = 0;
			}	
}

void boardCheck_Vertical(int board[][7], int &victor, bool &victory)
{
	/* Runs from top to bottom, left to right. */
	
	int row;
	int column;
	int number = 3;
	int stacks = 0;
	for(column = 6; column >= 0; column--)
			{
				for(row = 0; row < 6; row++)
				{
					checkChip(board, row, column, number, stacks, victor, victory);
				}
				stacks = 0;
			}
}

void boardCheck_DiagonalLEFT(int board[][7], int &victor, bool &victory)
{
	/* Runs from top left to bottom right. The checked space is shown below.
		1111000
		1111100
		0111110
		0011111
		0001111 */
		
	int row;
	int column;
	int diaRow;
	int diaColumn;
	int number = 3;
	int stacks = 0;
	for(row = 0; row <= 2; row++)
	{
		if(row == 0)
		{
			for(column = 0; column <= 3; column++)
			{
				diaColumn = column;
				diaRow = row;
				while(diaRow < 6 && diaColumn < 7)
				{
					checkChip(board, diaRow, diaColumn, number, stacks, victor, victory);
					diaRow++;
					diaColumn++;
				}
			}
		}
		else
		{
			column = 0;
			diaRow = row;
			while(diaRow < 6 && column < 7)
			{
				checkChip(board, diaRow, column, number, stacks, victor, victory);
				diaRow++;
				column++;
			}
		}
	}
}

void boardCheck_DiagonalRIGHT(int board[][7], int &victor, bool &victory)
{
	/* Runs from top right to bottom left. The checked space is shown below.
		0002222
		0022222
		0222220
		2222200
		2222000 */
	
	int row;
	int column;
	int diaRow;
	int diaColumn;
	int number = 3;
	int stacks = 0;
	for(row = 0; row <= 2; row++)
	{
		if(row == 0)
		{
			for(column = 6; column >= 3; column--)
			{
				diaColumn = column;
				diaRow = row;
				while(diaRow >= 0 && diaColumn >= 0)
				{
					checkChip(board, diaRow, diaColumn, number, stacks, victor, victory);
					diaRow++;
					diaColumn--;
				}
			}
		}
		else
		{
			column = 6;
			diaRow = row;
			while(diaRow >= 0 && column >= 0)
			{
				checkChip(board, diaRow, column, number, stacks, victor, victory);
				diaRow++;
				column--;
			}
		}
	}
}
